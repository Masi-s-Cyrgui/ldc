from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.spinner import Spinner
from kivy.uix.popup import Popup
import sqlite3
import os

class DatabaseManager:
    _instance = None

    def __new__(cls, app=None):
        if not cls._instance:
            cls._instance = super(DatabaseManager, cls).__new__(cls)
            if app:
                cls._instance.init_app(app)
        return cls._instance

    def init_app(self, app):
        data_dir = app.user_data_dir
        print(data_dir)
        db_path = os.path.join(data_dir, 'inventory.db')
        self.connection = sqlite3.connect(db_path)
        self.cursor = self.connection.cursor()
        self.create_tables()

    def create_tables(self):
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS tools (
                tool_id INTEGER PRIMARY KEY,
                tool_name TEXT NOT NULL
            )
        ''')

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS sites (
                site_id INTEGER PRIMARY KEY,
                site_name TEXT NOT NULL
            )
        ''')

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS tools_at_site (
                id INTEGER PRIMARY KEY,
                site_id INTEGER,
                tool_id INTEGER,
                quantity INTEGER NOT NULL,
                FOREIGN KEY (site_id) REFERENCES sites(site_id),
                FOREIGN KEY (tool_id) REFERENCES tools(tool_id)
            )
        ''')

        self.connection.commit()

    def add_tool(self, tool_name):
        self.cursor.execute('INSERT INTO tools (tool_name) VALUES (?)', (tool_name,))
        self.connection.commit()

    def add_site(self, site_name):
        self.cursor.execute('INSERT INTO sites (site_name) VALUES (?)', (site_name,))
        self.connection.commit()

    def add_tool_to_site(self, site_id, tool_id):
        self.cursor.execute('INSERT INTO tools_at_site (site_id, tool_id, quantity) VALUES (?, ?, ?)',
                            (site_id, tool_id, 0))
        self.connection.commit()

    def get_sites(self):
        self.cursor.execute('SELECT * FROM sites')
        return self.cursor.fetchall()

    def get_tools(self):
        self.cursor.execute('SELECT * FROM tools')
        return self.cursor.fetchall()

    def get_tools_at_site(self, site_id):
        query = '''
            SELECT tools.tool_name, tools_at_site.quantity
            FROM tools_at_site
            JOIN tools ON tools_at_site.tool_id = tools.tool_id
            WHERE tools_at_site.site_id = ?
        '''
        self.cursor.execute(query, (site_id,))
        return self.cursor.fetchall()
    
    def tool_exists_at_site(self, site_id, tool_id):
        query = 'SELECT * FROM tools_at_site WHERE site_id = ? AND tool_id = ?'
        self.cursor.execute(query, (site_id, tool_id))
        return self.cursor.fetchone() is not None
    
    def get_tool_id(self, tool_name):
        self.cursor.execute('SELECT tool_id FROM tools WHERE tool_name = ?', (tool_name,))
        result = self.cursor.fetchone()
        return result[0] if result else None
    
    def get_quantity_at_site(self, site_id, tool_name):
        query = '''
            SELECT quantity
            FROM tools_at_site
            WHERE site_id = ? AND tool_id = (SELECT tool_id FROM tools WHERE tool_name = ?)
        '''
        self.cursor.execute(query, (site_id, tool_name))
        result = self.cursor.fetchone()
        return result[0] if result else 0  # Default to 0 if no entry is found

    def update_quantity_at_site(self, site_id, tool_name, quantity):
        tool_id = self.get_tool_id(tool_name)
        query = '''
            UPDATE tools_at_site
            SET quantity = ?
            WHERE site_id = ? AND tool_id = ?
        '''
        self.cursor.execute(query, (quantity, site_id, tool_id))
        self.connection.commit()

    def tool_exists_at_site(self, site_id, tool_id):
        query = '''
            SELECT COUNT(*)
            FROM tools_at_site
            WHERE site_id = ? AND tool_id = ?
        '''
        self.cursor.execute(query, (site_id, tool_id))
        result = self.cursor.fetchone()
        return result[0] > 0
    

    def delete_site(self, site_id):
        self.cursor.execute('DELETE FROM sites WHERE site_id = ?', (site_id,))
        self.cursor.execute('DELETE FROM tools_at_site WHERE site_id = ?', (site_id,))
        self.connection.commit()

    def delete_tool_at_site(self, site_id, tool_name):
        self.cursor.execute('DELETE FROM tools_at_site WHERE site_id = ? AND tool_id = (SELECT tool_id FROM tools WHERE tool_name = ?)', (site_id, tool_name,))
        self.connection.commit()

    def close(self):
        self.connection.close()


class MainMenuScreen(Screen):
    def __init__(self, **kwargs):
        super(MainMenuScreen, self).__init__(**kwargs)

        layout = BoxLayout(orientation='vertical')
        add_tool_button = Button(text='Ajouter Matériel', on_press=self.go_to_add_tool_screen)
        add_site_button = Button(text='Ajouter Chantier', on_press=self.go_to_add_site_screen)
        view_sites_button = Button(text='Chantiers', on_press=self.go_to_list_sites_screen)

        layout.add_widget(add_tool_button)
        layout.add_widget(add_site_button)
        layout.add_widget(view_sites_button)

        self.add_widget(layout)

    def go_to_add_tool_screen(self, instance):
        self.manager.current = 'add_tool'

    def go_to_add_site_screen(self, instance):
        self.manager.current = 'add_site'

    def go_to_list_sites_screen(self, instance):
        self.manager.current = 'list_site'


class AddToolScreen(Screen):
    def __init__(self, db_manager, **kwargs):
        super(AddToolScreen, self).__init__(**kwargs)
        self.db_manager = db_manager

        layout = BoxLayout(orientation='vertical')
        tool_name_input = TextInput(hint_text='')
        add_button = Button(text='Ajouter Matériel', on_press=lambda x: self.add_tool(tool_name_input.text, tool_name_input))
        back_button = Button(text='Retour', on_press=self.go_to_main_menu)

        layout.add_widget(tool_name_input)
        layout.add_widget(add_button)
        layout.add_widget(back_button)

        self.add_widget(layout)


    def add_tool(self, tool_name, tool_name_input):
        self.db_manager.add_tool(tool_name)
        tool_name_input.text = ''

    def go_to_main_menu(self, instance):
        self.manager.current = 'main_menu'


class AddSiteScreen(Screen):
    def __init__(self, db_manager, **kwargs):
        super(AddSiteScreen, self).__init__(**kwargs)
        self.db_manager = db_manager

        layout = BoxLayout(orientation='vertical')
        site_name_input = TextInput(hint_text='')
        add_button = Button(text='Ajouter Chantier', on_press=lambda x: self.add_site(site_name_input.text, site_name_input))
        back_button = Button(text='Retour', on_press=self.go_to_main_menu)

        layout.add_widget(site_name_input)
        layout.add_widget(add_button)
        layout.add_widget(back_button)

        self.add_widget(layout)

    def add_site(self, site_name, site_name_input):
        self.db_manager.add_site(site_name)
        site_name_input.text = ''

    def go_to_main_menu(self, instance):
        self.manager.current = 'main_menu'


class SitesListScreen(Screen):
    def __init__(self, db_manager, **kwargs):
        super(SitesListScreen, self).__init__(**kwargs)
        self.db_manager = db_manager

        layout = BoxLayout(orientation='vertical')
        self.site_label = Label(text='Chantiers')
        self.site_list_layout = BoxLayout(orientation='vertical')
        self.back_button = Button(text='Retour', on_press=self.go_to_main_menu, size_hint_y=None, height=40)

        layout.add_widget(self.site_label)
        layout.add_widget(self.site_list_layout)
        layout.add_widget(self.back_button)

        self.add_widget(layout)

    def on_enter(self):
        self.refresh_site_list()

    def refresh_site_list(self):
        sites = self.db_manager.get_sites()
        self.site_list_layout.clear_widgets()

        for site in sites:
            site_layout = BoxLayout(orientation='horizontal', size_hint_y=None, height=40)
            site_button = Button(text=site[1], on_press=lambda x, name=site[1]: self.show_tools_at_site(name))
            delete_button = Button(text='Supprimer', on_press=lambda x, name=site[1]: self.show_delete_popup(name))

            site_layout.add_widget(site_button)
            site_layout.add_widget(delete_button)
            self.site_list_layout.add_widget(site_layout)

    def show_tools_at_site(self, site_name):
        tools_at_site_list_screen = self.manager.get_screen('tools_at_site_list')
        tools_at_site_list_screen.set_site_name(site_name)
        self.manager.current = 'tools_at_site_list'

    def show_delete_popup(self, site_name):
        content = BoxLayout(orientation='vertical')
        delete_label = Label(text=f'Supprimer chantier "{site_name}"?')
        buttons_layout = BoxLayout(spacing=10)

        cancel_button = Button(text='Annuler', on_press=lambda x: self.dismiss_popup())
        delete_button = Button(text='Supprimer', on_press=lambda x: self.delete_site(site_name))

        buttons_layout.add_widget(cancel_button)
        buttons_layout.add_widget(delete_button)

        content.add_widget(delete_label)
        content.add_widget(buttons_layout)

        self._popup = Popup(title='Supprimer chantier', content=content, size_hint=(None, None), size=(400, 200))
        self._popup.open()

    def delete_site(self, site_name):
        site_id = self.get_site_id(site_name)

        if site_id:
            self.db_manager.delete_site(site_id)
            self.refresh_site_list()

        self.dismiss_popup()

    def dismiss_popup(self):
        self._popup.dismiss()

    def get_site_id(self, site_name):
        self.db_manager.cursor.execute('SELECT site_id FROM sites WHERE site_name = ?', (site_name,))
        result = self.db_manager.cursor.fetchone()
        return result[0] if result else None

    def go_to_main_menu(self, instance):
        self.site_label.text = 'Site List'
        self.manager.current = 'main_menu'


class ToolsAtSiteListScreen(Screen):
    def __init__(self, db_manager, **kwargs):
        super(ToolsAtSiteListScreen, self).__init__(**kwargs)
        self.db_manager = db_manager

        layout = BoxLayout(orientation='vertical')
        self.site_label = Label(text='')
        self.site_list_layout = BoxLayout(orientation='vertical')
        self.back_button = Button(text='Retour', on_press=self.go_to_sites_list_screen, size_hint_y=None, height=40)

        layout.add_widget(self.site_label)
        layout.add_widget(self.site_list_layout)
        layout.add_widget(self.back_button)

        self.add_widget(layout)


    def set_site_name(self, site_name):
        self.site_label.text = f'{site_name}'
        self.refresh_tools_at_site_list(site_name)

    def refresh_tools_at_site_list(self, site_name):
        site_id = self.get_site_id(site_name)
        tools_at_site = self.db_manager.get_tools_at_site(site_id)
        self.site_list_layout.clear_widgets()

        for tool in tools_at_site:
            tool_layout = BoxLayout(orientation='horizontal', size_hint_y=None, height=40)
            tool_label = Label(text=f"{tool[0]} : {tool[1]}")
            increment_button = Button(text='+', on_press=lambda x, tool_name=tool[0], site_id=site_id: self.increment_quantity(tool_name, site_id))
            decrement_button = Button(text='-', on_press=lambda x, tool_name=tool[0], site_id=site_id: self.decrement_quantity(tool_name, site_id))
            delete_button = Button(text='Supprimer', on_press=lambda x, tool_name=tool[0], site_id=site_id: self.show_delete_tool_popup(tool_name, site_id))
            
            tool_layout.add_widget(tool_label)
            tool_layout.add_widget(decrement_button)
            tool_layout.add_widget(increment_button)
            tool_layout.add_widget(delete_button)
            self.site_list_layout.add_widget(tool_layout)

        add_tool_button = Button(text='Ajouter Matériel', on_press=lambda x: self.go_to_add_tool_to_site_screen(site_id), size_hint_y=None, height=40)
        self.site_list_layout.add_widget(add_tool_button)

    def go_to_sites_list_screen(self, instance):
        self.site_label.text = ''
        self.manager.current = 'list_site'

    def get_site_id(self, site_name):
        self.db_manager.cursor.execute('SELECT site_id FROM sites WHERE site_name = ?', (site_name,))
        result = self.db_manager.cursor.fetchone()
        return result[0] if result else None

    def increment_quantity(self, tool_name, site_id):
        current_quantity = self.db_manager.get_quantity_at_site(site_id, tool_name)
        new_quantity = current_quantity + 1
        self.db_manager.update_quantity_at_site(site_id, tool_name, new_quantity)
        self.refresh_tools_at_site_list(self.site_label.text)

    def decrement_quantity(self, tool_name, site_id):
        current_quantity = self.db_manager.get_quantity_at_site(site_id, tool_name)
        new_quantity = max(0, current_quantity - 1)  # Ensure quantity doesn't go below 0
        self.db_manager.update_quantity_at_site(site_id, tool_name, new_quantity)
        self.refresh_tools_at_site_list(self.site_label.text)
       
    def go_to_add_tool_to_site_screen(self, site_id):
        add_tool_to_site_screen = self.manager.get_screen('add_tool_to_site')
        add_tool_to_site_screen.site_id = site_id
        self.manager.current = 'add_tool_to_site'

    def show_delete_tool_popup(self, tool_name, site_id):
        content = BoxLayout(orientation='vertical')
        delete_label = Label(text=f'Supprimer matériel "{tool_name}"?')
        buttons_layout = BoxLayout(spacing=10)

        cancel_button = Button(text='Annuler', on_press=lambda x: self.dismiss_popup())
        delete_button = Button(text='Supprimer', on_press=lambda x: self.delete_tool(tool_name, site_id))

        buttons_layout.add_widget(cancel_button)
        buttons_layout.add_widget(delete_button)

        content.add_widget(delete_label)
        content.add_widget(buttons_layout)

        self._popup = Popup(title='Supprimer matériel', content=content, size_hint=(None, None), size=(400, 200))
        self._popup.open()

    def delete_tool(self, tool_name, site_id):
        self.db_manager.delete_tool_at_site(site_id, tool_name)
        self.refresh_tools_at_site_list(self.site_label.text)
        self.dismiss_popup()

    def dismiss_popup(self):
        self._popup.dismiss()


class AddToolToSiteScreen(Screen):
    site_id = None  # Add a class attribute for site_id

    def __init__(self, db_manager, **kwargs):
        super(AddToolToSiteScreen, self).__init__(**kwargs)
        self.db_manager = db_manager

        layout = BoxLayout(orientation='vertical')
        self.tool_spinner = Spinner(text='Matériels', values=self.get_tools_list())
        add_button = Button(text='Ajouter matériel au chantier', on_press=lambda x: self.add_tool_to_site(self.tool_spinner.text))
        back_button = Button(text='Retour', on_press=lambda x: self.go_to_tool_list_site_screen(self.site_id))

        layout.add_widget(self.tool_spinner)
        layout.add_widget(add_button)
        layout.add_widget(back_button)

        self.add_widget(layout)



    def on_enter(self):
        self.tool_spinner.values = self.get_tools_list()

    def get_tools_list(self):
        tools = self.db_manager.get_tools()
        return [tool[1] for tool in tools]

    def add_tool_to_site(self, tool_name):
        tool_id = self.get_tool_id(tool_name)
        site_id = self.site_id

        if tool_id and site_id:
            if not self.db_manager.tool_exists_at_site(site_id, tool_id):
                self.db_manager.add_tool_to_site(site_id, tool_id)
                self.manager.get_screen('list_site').refresh_site_list()
        self.tool_spinner.text = 'Matériels'

    def get_tool_id(self, tool_name):
        self.db_manager.cursor.execute('SELECT tool_id FROM tools WHERE tool_name = ?', (tool_name,))
        result = self.db_manager.cursor.fetchone()
        return result[0] if result else None

    def go_to_tool_list_site_screen(self, site_id):
        self.db_manager.cursor.execute('SELECT site_name FROM sites WHERE site_id = ?', (site_id,))
        site_name = self.db_manager.cursor.fetchone()
        tools_at_site_list_screen = self.manager.get_screen('tools_at_site_list')
        tools_at_site_list_screen.set_site_name(site_name[0])
        self.manager.current = 'tools_at_site_list'


class InventoryApp(App):


    def build(self):
        db_manager = DatabaseManager(self)
        sm = ScreenManager()
        sm.add_widget(MainMenuScreen(name='main_menu'))
        sm.add_widget(AddToolScreen(db_manager, name='add_tool'))
        sm.add_widget(AddSiteScreen(db_manager, name='add_site'))
        sm.add_widget(SitesListScreen(db_manager, name='list_site'))
        sm.add_widget(ToolsAtSiteListScreen(db_manager, name='tools_at_site_list'))
        sm.add_widget(AddToolToSiteScreen(db_manager, name='add_tool_to_site'))
        return sm


if __name__ == '__main__':
    InventoryApp().run()